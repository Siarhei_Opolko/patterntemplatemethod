﻿using System;

namespace TemplateMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            var hotDog = new HotDog();
            var hamburger = new Hamburger();

            Console.WriteLine("\nHot Dog:");
            hotDog.Prepare();

            Console.WriteLine("\nHamburger:");
            hamburger.Prepare();

            Console.ReadLine();
        }
    }

    internal class HotDog : FastFood
    {
        public override void PrepareMainComponent()
        {
            Console.WriteLine("Sousage");
        }

        public override void AddTopings()
        {
            Console.WriteLine("Mustard");
        }

        public override bool CustomerWantsTopings()
        {
            Console.WriteLine("Do you want mustard?");
            var answer = Console.ReadLine();

            return answer.ToLower() == "yes" || answer.ToLower() == "y";
        }
    }
    internal class Hamburger : FastFood
    {
        public override void PrepareMainComponent()
        {
            Console.WriteLine("Meat");
        }

        public override void AddTopings()
        {
            Console.WriteLine("Ketchup");
        }
    }
}
