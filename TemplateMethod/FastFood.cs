﻿using System;

namespace TemplateMethod
{
    abstract class FastFood
    {
        public void Prepare()
        {
            RoastBread();
            PrepareMainComponent();
            PutVegetables();
            
            if(CustomerWantsTopings())
                AddTopings();
        }

        public virtual bool CustomerWantsTopings()
        {
            return true;
        }

        public abstract void PrepareMainComponent();

        public abstract void AddTopings();
       

        public void RoastBread()
        {
            Console.WriteLine("Bread");
        }

        public void PutVegetables()
        {
            Console.WriteLine("Vegetables");
        }
    }
}
